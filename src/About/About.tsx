import React from 'react'
import styles from './About.module.scss'

const About = () => {
    return(
        <div id="about" className={styles.page}>
            <div className={styles.blockAbout}>
                <img alt="" className={styles.aboutLogo} src="aboutUs.png"/>
                <div className={styles.aboutText}>
                    We established in 1996 and are among the top three restaurants in the region.
                    All food is prepared in the kitchen by the best chefs in our city right inside the restaurant.<br/><br/>
                    In addition, in this beautiful place you can not only taste delicious dishes, but also enjoy a truly impressive atmosphere.<br/><br/>
                    Welcome to the Acropolis Secret.
                </div>
            </div>
            <img alt="" className={styles.aboutImg} src="aboutPhoto.png"/>
        </div>
    )
}

export default About