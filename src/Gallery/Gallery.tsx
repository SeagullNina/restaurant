import React from 'react';
import Carousel from './Carousel'
import styles from './Gallery.module.scss'

const Gallery = () => {
    return(
        <div id="gallery" className={styles.page}>
            <Carousel/>
        </div>
    )
}

export default Gallery

