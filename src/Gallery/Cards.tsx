import React from 'react';
import styles from './Cards.module.scss'

interface IProps{
    src: string;
}

const Cards = (props: IProps) => {
    return(
        <div className={styles.background}>
            <img src={props.src} alt="" className={styles.image}/>
        </div>
    )
}

export default Cards