import React from "react";
import styles from "./Carousel.module.scss";
import Slider from "react-slick";
import "./CarouselGallery.css";
import "slick-carousel/slick/slick-theme.css";
import "slick-carousel/slick/slick.css";
import classnames from "classnames";
import nanoid from "nanoid";
import Cards from "./Cards";

const pages = [
    {
        id: 1,
        src: "Photo1.png"
    }
  ,
    {
        id: 2,
        src: "Photo2.png"
    },
    {
        id: 3,
        src: "Photo3.png"
    },
    {
        id: 4,
        src: "Photo4.png"
    },
    {
        id: 5,
        src: "Photo5.png"
    },
    {
        id: 6,
        src: "Photo6.png"
    },
    {
        id: 7,
        src: "Photo7.png"
    }
    ,
    {
        id: 8,
        src: "Photo8.png"
    },
    {
        id: 9,
        src: "Photo1.png"
    },
    {
        id: 10,
        src: "Photo2.png"
    }
];


function NextArrow(props: any) {
    const { className, onClick } = props;
    return (
        <img src="./Right.png" alt="" onClick={onClick} className={classnames(styles.nextArrow, className)}/>
    );
}

function PrevArrow(props: any) {
    const { className, onClick } = props;
    return (
        <img src="./Left.png" alt="" onClick={onClick} className={classnames(styles.prevArrow, className)}/>
    );
}

const CarouselComponent = () => {

    const settings = {
        useCSS: true,
        dots: false,
        swipe: true,
        arrows: true,
        infinite: false,
        speed: 500,
        rows: 2,
        slidesPerRow: 1,
        slidesToShow: 4,
        slidesToScroll: 1,
        dotsClass: "slick-dots slick-thumb",
        nextArrow: <NextArrow />,
        prevArrow: <PrevArrow />,
        appendDots: (dots: any) => (
            <div>
                <ul className={styles.dotsBlock}> {dots} </ul>
            </div>
        ),
        responsive:[
            {
                breakpoint: 1200,
                settings:{
                    rows: 1,
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 1000,
                settings:{
                    rows: 1,
                    slidesToShow: 1
                }
            }
        ]
    };
    return (
        <Slider {...settings} className={styles.gallery}>
            {pages.map(photo => {
                return (
                    <Cards
                        key={photo.id}
                        src={photo.src}
                    />
                );
            })}
        </Slider>
    );
};

export default CarouselComponent;

