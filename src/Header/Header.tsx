import React from 'react'
import styles from './Header.module.scss'

const Header = () =>{
    return(
        <div id="home" className={styles.page}>
            <div className={styles.upHeader}>
                <img className={styles.miniLogo} src="./miniLogo.png" alt=""/>
                <div className={styles.menu}>
                    <div className={styles.menuItem}>
                        <a href="#home">HOME</a>
                    </div>
                    <div className={styles.menuItem}>
                        <a href="#about">ABOUT US</a>
                    </div>
                    <div className={styles.menuItem}>
                        <a href="#menu">MENU</a>
                    </div>
                    <div className={styles.menuItem}>
                        <a href="#reservations">RESERVATIONS</a>
                    </div>
                    <div className={styles.menuItem}>
                        <a href="#gallery">GALLERY</a>
                    </div>
                    <div className={styles.menuItem}>
                        <a href="#contact">CONTACT</a>
                    </div>
                </div>
            </div>
            <div className={styles.tagline}>
                <img className={styles.logo} src="./logo.png" alt=""/>
                <span className={styles.moto}>YOUR FAVORITE RESTAURANT</span>
            </div>
            <div className={styles.workingHours}>
                <span className={styles.string}><b>WORKING HOURS</b></span>
                <span className={styles.string}><b>Monday - Friday:</b> 8 A.M. - 11 P.M.</span>
                <span className={styles.string}><b>Weekends:</b> A.M. - 5 P.M.</span>
            </div>
        </div>
    )
}

export default Header