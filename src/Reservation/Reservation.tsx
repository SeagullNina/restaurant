import React from 'react'
import styles from './Reservation.module.scss'

const Reservation = () => {
    return(
        <div id="reservations" className={styles.page}>
            <img alt="" className={styles.reservationPhoto} src="reservation.png"/>
            <div className={styles.form}>
                <img alt="" className={styles.reservationLogo} src="reservationLogo.png"/>
                <input className={styles.input} type="text" placeholder="Your name"/>
                <input className={styles.input} type="text" placeholder="Your phone"/>
                <input className={styles.input} type="text" placeholder="Date"/>
                <input className={styles.input} type="text" placeholder="Number of persons"/>
                <input type="button" className={styles.button} value="RESERVE NOW"/>
            </div>
        </div>
    )
}

export default Reservation