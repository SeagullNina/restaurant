import React from "react";
import styles from './Contact.module.scss'

const Contact = () => {
    return(
        <div id="contact" className={styles.page}>
            <div className={styles.info}>
                <span className={styles.infoItem}>9 West Street, New York, NY 10128</span>
                <span className={styles.infoItem}>Reservation phone: (202) 123-4567</span>
                <span className={styles.infoItem}>Email: AcropSecret@gmail.com</span>
            </div>
            <div className={styles.links}>
                <img alt="" className={styles.sites} src="./facebook.png" onClick={()=>{ window.location.href = 'https://www.facebook.com'}}/>
                <img alt="" className={styles.sites} src="./instagram.png" onClick={()=>{ window.location.href = 'https://www.instagram.com'}}/>
                <img alt="" className={styles.sites} src="./twitter.png" onClick={()=>{ window.location.href = 'https://www.twitter.com'}}/>
                <img alt="" className={styles.sites} src="./pinterest.png"onClick={()=>{ window.location.href = 'https://www.pinterest.com'}}/>
            </div>
            <div className={styles.copyright}>© Acropolis Secret 1996-2019. All rights reserved.</div>
        </div>
    )
}

export default Contact