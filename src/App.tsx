import React from 'react';
import './App.css';
import Header from './Header/Header'
import About from './About/About'
import Menu from './Menu/Menu'
import MenuDishes from "./MenuDishes/MenuDishes";
import Reservation from "./Reservation/Reservation";
import Gallery from "./Gallery/Gallery";
import Contact from './Contact/Contact'

function App() {
  return (
    <div className="App">
      <Header/>
      <About/>
      <Menu/>
      <MenuDishes/>
      <Reservation/>
      <Gallery/>
      <Contact/>
    </div>
  );
}

export default App;
