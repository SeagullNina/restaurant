import React from 'react'
import styles from './Menu.module.scss'

const Menu = () => {
    return(
        <div id="menu" className={styles.page}>
            <img alt="" className={styles.menuLogo} src="../menuLogo.png"/>
            <img alt="" className={styles.menuItems} src="../menuItems.png"/>
        </div>
    )
}

export default Menu