import React from "react";
import styles from "./Carousel.module.scss";
import Cards from "./Cards";
import Slider from "react-slick";
import './CarouselMenu.css'
import "slick-carousel/slick/slick-theme.css";
import "slick-carousel/slick/slick.css";
import classnames from 'classnames'

const pages= [
  {
    id: 1,
    src: "dish1.png",
    name: "BELGIAN WAFFLES WITH ICE CREAM AND BERRIES",
    description:
      "Traditional Belgian waffles with raspberries and blueberries; " +
      "Served with a scoop of ice cream to choose from (strawberry, vanilla, chocolate).",
    price: "6$/350g"
  },
  {
    id: 2,
    src: "dish2.png",
    name: "CHOCOLATE PUDDING",
    description:
      "Airy chocolate pudding with shabby walnuts, served with chocolate syrup",
    price: "7$/420g"
  },
  {
    id: 3,
    src: "dish3.png",
    name: "CUPCAKE",
    description:
      "Semi-chocolate cake in 2 layers, stuffed with a banana and sprinkled with pieces of milk chocolate.",
    price: "3$/150g"
  },
  {
    id: 4,
    src: "dish4.png",
    name: "PANCAKES WITH HONEY AND BERRIES",
    description:
      "Homemade pancakes decorated with blueberries, served with honey.",
    price: "8.5$/500g"
  },
  {
    id: 1,
    src: "dish2.png",
    name: "CHOCOLATE PUDDING",
    description:
      "Airy chocolate pudding with shabby walnuts, served with chocolate syrup",
    price: "7$/420g"
  },
  {
    id: 2,
    src: "dish1.png",
    name: "BELGIAN WAFFLES WITH ICE CREAM AND BERRIES",
    description:
      "Traditional Belgian waffles with raspberries and blueberries; " +
      "Served with a scoop of ice cream to choose from (strawberry, vanilla, chocolate).",
    price: "6$/350g"
  },
  {
    id: 3,
    src: "dish4.png",
    name: "PANCAKES WITH HONEY AND BERRIES",
    description:
      "Homemade pancakes decorated with blueberries, served with honey.",
    price: "8.5$/500g"
  },
  {
    id: 4,
    src: "dish3.png",
    name: "CUPCAKE",
    description:
      "Semi-chocolate cake in 2 layers, stuffed with a banana and sprinkled with pieces of milk chocolate.",
    price: "3$/150g"
  }
];

function NextArrow(props: any) {
  const { className, onClick } = props;
  return (
      <img src="./Right.png" alt="" onClick={onClick} className={classnames(styles.nextArrow, className)}/>
  );
}

function PrevArrow(props: any) {
  const { className, onClick } = props;
  return (
      <img src="./Left.png" alt="" onClick={onClick} className={classnames(styles.prevArrow, className)}/>
  );
}

const CarouselComponent = () => {

  const settings = {
    useCSS: true,
    dots: true,
    swipe: true,
    arrows: true,
    infinite: false,
    speed: 500,
    rows: 2,
    slidesPerRow: 1,
    slidesToShow: 2,
    slidesToScroll: 1,
    dotsClass: "slick-dots slick-thumb",
    nextArrow: <NextArrow />,
    prevArrow: <PrevArrow />,
    appendDots: (dots: any) => (
        <div>
          <ul className={styles.dotsBlock}> {dots} </ul>
        </div>
    ),
    responsive:[
      {
        breakpoint: 1200,
        settings:
            {
              slidesToShow: 1,
              rows: 1
            }
      }

    ]
  };
  return (
    <Slider {...settings} className={styles.dishes}>
      {pages.map(dish => {
        return (
          <Cards
            key={dish.id}
            name={dish.name}
            description={dish.description}
            price={dish.price}
            src={dish.src}
          />
        );
      })}
    </Slider>
  );
};

export default CarouselComponent;
