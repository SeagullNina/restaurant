import React from 'react';
import styles from './Cards.module.scss'

interface IProps{
    src: string;
    name: string;
    description: string;
    price: string;
}

const Cards = (props: IProps) => {
    return(
        <div className={styles.background}>
            <img src={props.src} alt="" className={styles.image}/>
            <div className={styles.info}>
                <span className={styles.name}><b>{props.name}</b></span>
                <span className={styles.description}>{props.description}</span>
                <span className={styles.price}><b>{props.price}</b></span>
            </div>
        </div>
    )
}

export default Cards