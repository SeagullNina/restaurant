import React from 'react';
import styles from './MenuDishes.module.scss'
import Carousel from "./Carousel";

const MenuDishes = () => {
    return(
        <div className={styles.page}>
            <Carousel/>
        </div>
    )
}

export default MenuDishes

